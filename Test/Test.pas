unit Test;

interface

uses
  System.SysUtils, System.Variants, System.Classes, Generics.Collections, Generics.Defaults, types, math, strutils,
  Vcl.StdCtrls,
  SSIndex;

////////////////////////////////////////////////////////////////////////////////
///                     テスト用クラス定義ここから                           ///
////////////////////////////////////////////////////////////////////////////////
type
TParson = class
private
  Fname: string;
  Fage:  integer;
public
  constructor Create(name: string; age: integer);
  destructor  Destroy;override;
  function    Equals(other: TParson): boolean;
end;

TParsonComparer = class(TComparer<TParson>)
public
  function Compare(const Left, Right: TParson): Integer; override;
end;

TParsonEqualityComparer = class(TEqualityComparer<TParson>)
public
  function Equals(const Left, Right: TParson): boolean; override;
  function GetHashCode(const Left: TParson): integer; override;
end;

////////////////////////////////////////////////////////////////////////////////
///                     テスト用クラス定義ここまで                           ///
////////////////////////////////////////////////////////////////////////////////


type TestTSSIndex = class
public
  class procedure Search(memo: TMemo);
  class procedure Sort(memo: TMemo);
end;

implementation

class procedure TestTSSIndex.Search(memo: TMemo);
var
  ss: TSSIndex<TParson>;
  parsons: array of TParson;
  i: integer;
  indexs: TArray<integer>;
  list: TList<TParson>;
begin
  setlength(parsons, 6);
  parsons[0] := TParson.Create('田中', 10);
  parsons[1] := TParson.Create('山田', 17);
  parsons[2] := TParson.Create('鈴木', 25);
  parsons[3] := TParson.Create('佐藤', 3);
  parsons[4] := TParson.Create('鈴木', 25);
  parsons[5] := TParson.Create('山田', 21);

  ss := TSSIndex<TParson>.Create(parsons, TParsonEqualityComparer.Create, TParsonComparer.Create);
  try
    for i in ss.Search(parsons[2]) do memo.Lines.Add(i.ToString);
  finally
    ss.Free;
  end;

  for i := 0 to High(parsons) do
    parsons[i].Free;
end;

class procedure TestTSSIndex.Sort(memo: TMemo);
var
  ss: TSSIndex<TParson>;
  parsons: array of TParson;
  i: integer;
  indexs: TArray<integer>;
begin
  setlength(parsons, 6);
  parsons[0] := TParson.Create('田中', 10);
  parsons[1] := TParson.Create('山田', 17);
  parsons[2] := TParson.Create('鈴木', 25);
  parsons[3] := TParson.Create('佐藤', 3);
  parsons[4] := TParson.Create('鈴木', 25);
  parsons[5] := TParson.Create('山田', 21);

  ss := TSSIndex<TParson>.Create(parsons, TParsonEqualityComparer.Create, TParsonComparer.Create);
  try
    for i in ss.Sort() do memo.Lines.Add(i.ToString);
  finally
    ss.Free;
  end;

  for i := 0 to High(parsons) do
    parsons[i].Free;
end;

////////////////////////////////////////////////////////////////////////////////
///                       テスト用クラスここから                             ///
////////////////////////////////////////////////////////////////////////////////
constructor TParson.Create(name: string; age: integer);
begin
  FName := name;
  FAge  := age;
end;

destructor TParson.Destroy;
begin
  inherited;
end;

function TParson.Equals(other: TParson): boolean;
begin
  result := true;

  if       Fname <> other.Fname then result := false
  else if  Fage  <> other.Fage  then result := false;
end;

function TParsonComparer.Compare(const Left, Right: TParson): integer;
begin
  result := Left.Fage - Right.Fage;
end;

function TParsonEqualityComparer.Equals(const Left, Right: TParson): boolean;
begin
  result := Left.Equals(Right);
end;

function TParsonEqualityComparer.GetHashCode(const Left: TParson): integer;
begin
  result := 0;  //いいのかなぁ...
end;

////////////////////////////////////////////////////////////////////////////////
///                       テスト用クラスここまで                             ///
////////////////////////////////////////////////////////////////////////////////

end.
