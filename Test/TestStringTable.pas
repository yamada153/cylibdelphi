unit TestStringTable;

interface

uses
  Vcl.StdCtrls, System.SysUtils, System.Variants, System.Classes, Generics.Collections, Generics.Defaults, types,
  StringTable;

type
TTestStringTable = class
public
  class procedure InsertField(memo: TMemo);
  class procedure ChangeField(memo: TMemo);
  class procedure DeleteField(memo: TMemo);
  class procedure InsertRow(memo: TMemo);
  class procedure DeleteRow(memo: TMemo);
end;

implementation

{ TTestStringTable }

class procedure TTestStringTable.InsertField(memo: TMemo);
var
  st : TStringTable;
  copy_st: TStringTable;
begin
  st := TStringTable.Create(['test1', 'test2', 'test3']);
  try
    st.Add([ 'value1', 'value2', 'value3' ]);
    st.Add([ 'value4', 'value5', 'value6' ]);
    st.Add([ 'value7', 'value8', 'value9' ]);

    copy_st := TStringTable.Create(st); try copy_st.InsertField(-1, 'test4');memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertField(0, 'test4'); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertField(1, 'test4'); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertField(2, 'test4'); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertField(3, 'test4'); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertField(4, 'test4'); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;

//以下エラーになるコード
//    copy_st := TStringTable.Create(st); try copy_st.InsertField(1, '');     memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//    copy_st := TStringTable.Create(st); try copy_st.InsertField(1, ' ');    memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//    copy_st := TStringTable.Create(st); try copy_st.InsertField(1, #9);     memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//    copy_st := TStringTable.Create(st); try copy_st.InsertField(1, 'test1');memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;

  finally
    st.Free;
  end;
end;

class procedure TTestStringTable.ChangeField(memo: TMemo);
var
  st : TStringTable;
  copy_st: TStringTable;
begin
  st := TStringTable.Create(['test1', 'test2', 'test3']);
  try
    st.Add([ 'value1', 'value2', 'value3' ]);
    st.Add([ 'value4', 'value5', 'value6' ]);
    st.Add([ 'value7', 'value8', 'value9' ]);

    copy_st := TStringTable.Create(st); try copy_st.ChangeField('test1', 'てすと1');memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//以下エラー
//    copy_st := TStringTable.Create(st); try copy_st.ChangeField('', 'test');        memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//    copy_st := TStringTable.Create(st); try copy_st.ChangeField('test1', '');       memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//    copy_st := TStringTable.Create(st); try copy_st.ChangeField('てすと1', 'test1');memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
  finally
    st.Free;
  end;
end;

class procedure TTestStringTable.DeleteField(memo: TMemo);
var
  st : TStringTable;
  copy_st: TStringTable;
begin
  st := TStringTable.Create(['test1', 'test2', 'test3']);
  try
    st.Add([ 'value1', 'value2', 'value3' ]);
    st.Add([ 'value4', 'value5', 'value6' ]);
    st.Add([ 'value7', 'value8', 'value9' ]);

    copy_st := TStringTable.Create(st); try copy_st.DeleteField('test1'); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//以下エラー
//    copy_st := TStringTable.Create(st); try copy_st.DeleteField('TesT1'); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//    copy_st := TStringTable.Create(st); try copy_st.DeleteField('');      memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//    copy_st := TStringTable.Create(st); try copy_st.DeleteField('test１');memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;

  finally
    st.Free;
  end;
end;

class procedure TTestStringTable.InsertRow(memo: TMemo);
var
  st : TStringTable;
  copy_st: TStringTable;
begin
  st := TStringTable.Create(['test1', 'test2', 'test3']);
  try
    st.Add([ 'value1', 'value2', 'value3' ]);
    st.Add([ 'value4', 'value5', 'value6' ]);
    st.Add([ 'value7', 'value8', 'value9' ]);

    copy_st := TStringTable.Create(st); try copy_st.InsertRow(-1,['add']); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertRow(0, ['add']); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertRow(1, ['add']); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertRow(2, ['add']); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertRow(3, ['add']); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.InsertRow(4, ['add']); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;

  finally
    st.Free;
  end;
end;

class procedure TTestStringTable.DeleteRow(memo: TMemo);
var
  st : TStringTable;
  copy_st: TStringTable;
begin
  st := TStringTable.Create(['test1', 'test2', 'test3']);
  try
    st.Add([ 'value1', 'value2', 'value3' ]);
    st.Add([ 'value4', 'value5', 'value6' ]);
    st.Add([ 'value7', 'value8', 'value9' ]);

    copy_st := TStringTable.Create(st); try copy_st.DeleteRow(0); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.DeleteRow(1); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
    copy_st := TStringTable.Create(st); try copy_st.DeleteRow(2); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//以下エラー
//    copy_st := TStringTable.Create(st); try copy_st.DeleteRow(-1);memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
//    copy_st := TStringTable.Create(st); try copy_st.DeleteRow(3); memo.lines.Add(copy_st.ToString() + #13#10); finally copy_st.Free; end;
  finally
    st.Free;
  end;
end;

end.
