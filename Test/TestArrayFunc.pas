unit TestArrayFunc;

interface

uses
  Vcl.StdCtrls, System.SysUtils, System.Variants, System.Classes, Generics.Collections, Generics.Defaults, types,
  IdHashMessageDigest,
  ArrayFunc;

////////////////////////////////////////////////////////////////////////////////
///                     テスト用クラス定義ここから                           ///
////////////////////////////////////////////////////////////////////////////////
type
TPerson = class
private
  Fname: string;
  Fage:  integer;
public
  constructor Create(name: string; age: integer);
  destructor  Destroy;override;
  function    ToString: string;
end;

TPersonC = class(TInterfacedObject, IComparer<TPerson>)
public
  function Compare(const Left, Right: TPerson): Integer;
end;

TPersonEC = class(TInterfacedObject, IEqualityComparer<TPerson>)
public
  function Equals(const Left, Right: TPerson): boolean;
  function GetHashCode(const Left: TPerson): integer;
end;
////////////////////////////////////////////////////////////////////////////////
///                     テスト用クラス定義ここまで                           ///
////////////////////////////////////////////////////////////////////////////////

TTestArrayFunc = class
public
  class procedure Count(memo: TMemo);
  class procedure Contains(memo: TMemo);
  class procedure Concat(memo: TMemo);
  class procedure Reverse(memo: TMemo);
  class procedure Rotate(memo: TMemo);
  class procedure Compaction(memo: TMemo);
  class procedure Distinct(memo: TMemo);
  class procedure AndSet(memo: TMemo);
  class procedure SubSet(memo: TMemo);
  class procedure Split(memo: TMemo);
  class procedure Double(memo: TMemo);
  class procedure Equal(memo: TMemo);
end;

implementation

{ TTestArrayFunc }

class procedure TTestArrayFunc.Equal(memo: TMemo);
var
  nums1, nums2, nums3, nums4, nums5: TArray<integer>;
  persons1, persons2, persons3: array of TPerson;
  ires: integer;
  i, j: integer;
begin
  memo.Lines.Add('integer型でテスト');
  setlength(nums1, 5);
  nums1[0] := 7;
  nums1[1] := 2;
  nums1[2] := 5;
  nums1[3] := 1;
  nums1[4] := 5;
  setlength(nums2, 5);
  nums2[0] := 7;
  nums2[1] := 2;
  nums2[2] := 5;
  nums2[3] := 1;
  nums2[4] := 5;
  memo.Lines.Add('一致する：' + TArrayFunc.Equal<integer>(nums1, nums2).ToString);
  setlength(nums3, 5);
  nums3[0] := 5;
  nums3[1] := 1;
  nums3[2] := 5;
  nums3[3] := 2;
  nums3[4] := 7;
  memo.Lines.Add('逆にすれば一致する：' + TArrayFunc.Equal<integer>(nums1, nums3).ToString);
  setlength(nums4, 5);
  nums4[0] := 5;
  nums4[1] := 1;
  nums4[2] := 3;
  nums4[3] := 2;
  nums4[4] := 7;
  memo.Lines.Add('一致しない：' + TArrayFunc.Equal<integer>(nums1, nums4).ToString);
  setlength(nums5, 5);
  nums5[0] := 7;
  nums5[1] := 2;
  memo.Lines.Add('一致しない：' + TArrayFunc.Equal<integer>(nums1, nums5).ToString);

  memo.Lines.Add('クラス型でテスト');
  setlength(persons1, 5);
  persons1[0] := TPerson.Create('佐藤', 7);
  persons1[1] := TPerson.Create('山田', 18);
  persons1[2] := TPerson.Create('鈴木', 4);
  persons1[3] := TPerson.Create('斉藤', 0);
  persons1[4] := TPerson.Create('山田', 18);
  setlength(persons2, 5);
  persons2[0] := TPerson.Create('佐藤', 7);
  persons2[1] := TPerson.Create('山田', 18);
  persons2[2] := TPerson.Create('鈴木', 4);
  persons2[3] := TPerson.Create('斉藤', 0);
  persons2[4] := TPerson.Create('山田', 18);
  memo.Lines.Add('一致する：' + TArrayFunc.Equal<TPerson>(persons1, persons2, TPersonEC.Create).ToString);
  setlength(persons3, 5);
  persons3[0] := TPerson.Create('佐藤', 7);
  persons3[1] := TPerson.Create('山田', 18);
  persons3[2] := TPerson.Create('鈴木', 4);
  persons3[3] := TPerson.Create('斉', 0);
  persons3[4] := TPerson.Create('山田', 18);
  memo.Lines.Add('一致しない：' + TArrayFunc.Equal<TPerson>(persons1, persons3, TPersonEC.Create).ToString);


  for i := 0 to High(persons1) do persons1[i].Free;
  for i := 0 to High(persons2) do persons2[i].Free;
  for i := 0 to High(persons3) do persons3[i].Free;

end;

class procedure TTestArrayFunc.Double(memo: TMemo);
var
  nums, ires: TArray<integer>;
  persons: array of TPerson;
  pres: TArray<TPerson>;
  i, j: integer;
begin
  memo.Lines.Add('integer型でテスト');
  setlength(nums, 5);
  nums[0] := 7;
  nums[1] := 1;
  nums[2] := 5;
  nums[3] := 1;
  nums[4] := 5;
  ires := TArrayFunc.Double<integer>(nums);
  for i := 0 to High(ires) do memo.Lines.Add(ires[i].ToString);

  memo.Lines.Add('クラス型でテスト');
  setlength(persons, 5);
  persons[0] := TPerson.Create('佐藤', 7);
  persons[1] := TPerson.Create('山田', 18);
  persons[2] := TPerson.Create('鈴木', 4);
  persons[3] := TPerson.Create('斉藤', 0);
  persons[4] := TPerson.Create('山田', 18);
  pres := TArrayFunc.Double<TPerson>(persons, TPersonEC.Create);
  for i := 0 to High(pres) do memo.Lines.Add(pres[i].ToString);


  for i := 0 to High(persons) do persons[i].Free;
end;

class procedure TTestArrayFunc.Split(memo: TMemo);
var
  nums: TArray<integer>;
  itarget: integer;
  persons: array of TPerson;
  target: TPerson;
  personsplited: TArray<TArray<TPerson>>;
  isplited: TArray<TArray<integer>>;
  i, j: integer;
begin
  memo.Lines.Add('integer型でテスト');
  setlength(nums, 5);
  nums[0] := 7;
  nums[1] := 1;
  nums[2] := 5;
  nums[3] := 1;
  nums[4] := 3;

  itarget := 5;
  isplited := TArrayFunc.Split<integer>(nums, itarget);
  for i := 0 to High(isplited) do begin
    for j := 0 to High(isplited[i]) do begin
      memo.Lines.Add(isplited[i][j].ToString);
    end;
    memo.Lines.Add('--------------------------------------');
  end;

  memo.Lines.Add('クラス型でテスト');
  setlength(persons, 5);
  persons[0] := TPerson.Create('佐藤', 7);
  persons[1] := TPerson.Create('山田', 18);
  persons[2] := TPerson.Create('鈴木', 4);
  persons[3] := TPerson.Create('斉藤', 0);
  persons[4] := TPerson.Create('山田', 18);

  target := TPerson.Create('山田', 18);
  personsplited := TArrayFunc.Split<TPerson>(persons, target, TPersonEC.Create);
  for i := 0 to High(personsplited) do begin
    for j := 0 to High(personsplited[i]) do begin
      memo.Lines.Add(personsplited[i][j].ToString);
    end;
    memo.Lines.Add('--------------------------------------');
  end;
  target.Free;

  for i := 0 to High(persons) do persons[i].Free;
end;

class procedure TTestArrayFunc.SubSet(memo: TMemo);
var
  persons1, persons2: array of TPerson;
  nums1, nums2: TArray<integer>;
  numand: TArray<integer>;
  con: TArray<TPerson>;
  i: integer;
begin
  memo.Lines.Add('integer型でテスト');
  setlength(nums1, 3);
  nums1[0] := 7;
  nums1[1] := 1;
  nums1[2] := 9;

  setlength(nums2, 5);
  nums2[0] := 60;
  nums2[1] := 1;
  nums2[2] := 5;
  nums2[3] := 7;
  nums2[4] := 3;

  numand := TArrayFunc.SubSet<integer>(nums1, nums2);
  for i := 0 to High(numand) do memo.Lines.Add(numand[i].ToString);

  memo.Lines.Add('クラスでテスト');
  setlength(persons1, 5);
  persons1[0] := TPerson.Create('佐藤', 0);
  persons1[1] := TPerson.Create('山田', 1);
  persons1[2] := TPerson.Create('鈴木', 2);
  persons1[3] := TPerson.Create('斉藤', 3);
  persons1[4] := TPerson.Create('山田', 4);

  setlength(persons2, 2);
  persons2[0] := TPerson.Create('鈴木', 2);
  persons2[1] := TPerson.Create('後藤', 6);

  con := TArrayFunc.SubSet<TPerson>(persons2, persons1, TPersonEC.Create);
  for i := 0 to High(con) do memo.Lines.Add(con[i].ToString);

  for i := 0 to High(persons1) do persons1[i].Free;
  for i := 0 to High(persons2) do persons2[i].Free;
end;

class procedure TTestArrayFunc.AndSet(memo: TMemo);
var
  persons1, persons2: array of TPerson;
  nums1, nums2: TArray<integer>;
  numand: TArray<integer>;
  con: TArray<TPerson>;
  i: integer;
begin
  memo.Lines.Add('integer型でテスト');
  setlength(nums1, 3);
  nums1[0] := 7;
  nums1[1] := 1;
  nums1[2] := 9;

  setlength(nums2, 5);
  nums2[0] := 60;
  nums2[1] := 1;
  nums2[2] := 5;
  nums2[3] := 7;
  nums2[4] := 3;

  numand := TArrayFunc.AndSet<integer>(nums1, nums2);
  for i := 0 to High(numand) do memo.Lines.Add(numand[i].ToString);

  memo.Lines.Add('クラスでテスト');
  setlength(persons1, 5);
  persons1[0] := TPerson.Create('佐藤', 0);
  persons1[1] := TPerson.Create('山田', 1);
  persons1[2] := TPerson.Create('鈴木', 2);
  persons1[3] := TPerson.Create('斉藤', 3);
  persons1[4] := TPerson.Create('山田', 4);

  setlength(persons2, 2);
  persons2[0] := TPerson.Create('鈴木', 2);
  persons2[1] := TPerson.Create('後藤', 6);

  con := TArrayFunc.AndSet<TPerson>(persons2, persons1, TPersonEC.Create);
  for i := 0 to High(con) do memo.Lines.Add(con[i].ToString);

  for i := 0 to High(persons1) do persons1[i].Free;
  for i := 0 to High(persons2) do persons2[i].Free;
end;


class procedure TTestArrayFunc.Compaction(memo: TMemo);
var
  persons: array of TPerson;
  rot: TArray<TPerson>;
  i: integer;
begin
  setlength(persons, 5);
  persons[0] := TPerson.Create('佐藤', 0);
//  persons[1] := TPerson.Create('山田', 1);
  persons[2] := TPerson.Create('鈴木', 2);
  persons[3] := TPerson.Create('斉藤', 3);
//  persons[4] := TPerson.Create('山田', 4);

  memo.Lines.Add('//////////// コンパクション ////////////');
  rot := TArrayFunc.Compaction<TPerson>(persons, TEqualityComparer<TPerson>.Default);
  for i := 0 to High(rot) do memo.Lines.Add(rot[i].ToString);

  for i := 0 to High(persons) do persons[i].Free;
end;

class procedure TTestArrayFunc.Distinct(memo: TMemo);
var
  nums: TArray<integer>;
  persons: array of TPerson;
  dis: TArray<TPerson>;
  i: integer;
begin
  memo.Lines.Add('integer型でテスト');
  setlength(nums, 5);
  nums[0] := 7;
  nums[1] := 1;
  nums[2] := 5;
  nums[3] := 1;
  nums[4] := 3;
  nums := TArrayFunc.Distinct<integer>(nums);
  for i := 0 to High(nums) do memo.Lines.Add(nums[i].ToString);

  memo.Lines.Add('クラスでテスト');
  setlength(persons, 5);
  persons[0] := TPerson.Create('佐藤', 0);
  persons[1] := TPerson.Create('山田', 4);
  persons[2] := TPerson.Create('鈴木', 2);
  persons[3] := TPerson.Create('斉藤', 3);
  persons[4] := TPerson.Create('山田', 4);

  dis := TArrayFunc.Distinct<TPerson>(persons, TPersonEC.Create);
  for i := 0 to High(dis) do memo.Lines.Add(dis[i].ToString);

  for i := 0 to High(persons) do persons[i].Free;
end;

class procedure TTestArrayFunc.Rotate(memo: TMemo);
var
  persons: array of TPerson;
  rot: TArray<TPerson>;
  i: integer;
begin
  setlength(persons, 5);
  persons[0] := TPerson.Create('佐藤', 0);
  persons[1] := TPerson.Create('山田', 1);
  persons[2] := TPerson.Create('鈴木', 2);
  persons[3] := TPerson.Create('斉藤', 3);
  persons[4] := TPerson.Create('山田', 4);

  for i := 0 to High(persons) do memo.Lines.Add(persons[i].ToString);

  memo.Lines.Add('//////////// ローテーション ////////////');
  rot := TArrayFunc.Rotate<TPerson>(persons, 3);
  for i := 0 to High(rot) do memo.Lines.Add(rot[i].ToString);

  for i := 0 to High(persons) do persons[i].Free;
end;

class procedure TTestArrayFunc.Concat(memo: TMemo);
var
  persons1, persons2: array of TPerson;
  con: TArray<TPerson>;
  i: integer;
begin
  setlength(persons1, 5);
  persons1[0] := TPerson.Create('佐藤', 0);
  persons1[1] := TPerson.Create('山田', 1);
  persons1[2] := TPerson.Create('鈴木', 2);
  persons1[3] := TPerson.Create('斉藤', 3);
  persons1[4] := TPerson.Create('山田', 4);

  setlength(persons2, 2);
  persons2[0] := TPerson.Create('前田', 5);
  persons2[1] := TPerson.Create('後藤', 6);

  con := TArrayFunc.Concat<TPerson>(persons2, persons1);
  for i := 0 to High(con) do memo.Lines.Add(con[i].ToString);

  for i := 0 to High(con) do con[i].Free;
end;

//Reverseと少々の注意点
class procedure TTestArrayFunc.Reverse(memo: TMemo);
var
  persons: array of TPerson;
  rev: TArray<TPerson>;
  i: integer;
begin
  setlength(persons, 5);
  persons[0] := TPerson.Create('佐藤', 7);
  persons[1] := TPerson.Create('山田', 18);
  persons[2] := TPerson.Create('鈴木', 4);
  persons[3] := TPerson.Create('斉藤', 0);
  persons[4] := TPerson.Create('山田', 18);

  for i := 0 to High(persons) do memo.Lines.Add(persons[i].ToString);

  rev := TArrayFunc.Copy<TPerson>(persons); //Reverseは配列を逆転させてしまうので、逆転前を取得しておく

  memo.Lines.Add('//////////// 逆転 ////////////');
  TArrayFunc.Reverse<TPerson>(persons);
  for i := 0 to High(persons) do memo.Lines.Add(persons[i].ToString);

  memo.Lines.Add('//////////// 逆転前 ////////////');
  for i := 0 to High(rev) do memo.Lines.Add(rev[i].ToString);

  //もう使い終わったし、解放しよう
  for i := 0 to High(persons) do persons[i].Free;

  //逆転前のも解放しよう!!
//エラーになるrevが指している先のインスタンスは削除されているから  for i := 0 to High(rev) do rev[i].Free;
end;

class procedure TTestArrayFunc.Contains(memo: TMemo);
var
  nums: TArray<integer>;
  itarget: integer;
  persons: array of TPerson;
  target: TPerson;
  i: integer;
begin
  memo.Lines.Add('integer型でテスト');
  setlength(nums, 5);
  nums[0] := 7;
  nums[1] := 1;
  nums[2] := 5;
  nums[3] := 1;
  nums[4] := 3;

  itarget := 1; memo.lines.add(TArrayFunc.Contains<integer>(nums, itarget).ToString(TUseBoolStrs.True));
  itarget := 0; memo.lines.add(TArrayFunc.Contains<integer>(nums, itarget).ToString(TUseBoolStrs.True));

  memo.Lines.Add('クラス型でテスト');
  setlength(persons, 5);
  persons[0] := TPerson.Create('佐藤', 7);
  persons[1] := TPerson.Create('山田', 18);
  persons[2] := TPerson.Create('鈴木', 4);
  persons[3] := TPerson.Create('斉藤', 0);
  persons[4] := TPerson.Create('山田', 18);

  target := TPerson.Create('山田', 18); memo.lines.add(TArrayFunc.Contains<TPerson>(persons, target, TPersonEC.Create).tostring(TUseBoolStrs.True)); target.Free;
  target := TPerson.Create('山田', 0 ); memo.lines.add(TArrayFunc.Contains<TPerson>(persons, target, TPersonEC.Create).tostring(TUseBoolStrs.True)); target.Free;
  target := TPerson.Create('斉藤', 0 ); memo.lines.add(TArrayFunc.Contains<TPerson>(persons, target, TPersonEC.Create).tostring(TUseBoolStrs.True)); target.Free;

  for i := 0 to High(persons) do persons[i].Free;
end;

class procedure TTestArrayFunc.Count(memo: TMemo);
var
  nums: TArray<integer>;
  itarget: integer;
  persons: array of TPerson;
  target: TPerson;
  i: integer;
begin
  memo.Lines.Add('integer型でテスト');
  setlength(nums, 5);
  nums[0] := 7;
  nums[1] := 1;
  nums[2] := 5;
  nums[3] := 1;
  nums[4] := 3;

  itarget := 1; memo.lines.add(TArrayFunc.Count<integer>(nums, itarget).ToString);
  itarget := 0; memo.lines.add(TArrayFunc.Count<integer>(nums, itarget).ToString);

  memo.Lines.Add('クラス型でテスト');
  setlength(persons, 5);
  persons[0] := TPerson.Create('佐藤', 7);
  persons[1] := TPerson.Create('山田', 18);
  persons[2] := TPerson.Create('鈴木', 4);
  persons[3] := TPerson.Create('斉藤', 0);
  persons[4] := TPerson.Create('山田', 18);

  target := TPerson.Create('山田', 18); memo.lines.add(TArrayFunc.Count<TPerson>(persons, target, TPersonEC.Create).tostring); target.Free;
  target := TPerson.Create('山田', 0 ); memo.lines.add(TArrayFunc.Count<TPerson>(persons, target, TPersonEC.Create).tostring); target.Free;
  target := TPerson.Create('斉藤', 0 ); memo.lines.add(TArrayFunc.Count<TPerson>(persons, target, TPersonEC.Create).tostring); target.Free;

  for i := 0 to High(persons) do persons[i].Free;
end;

////////////////////////////////////////////////////////////////////////////////
///                       テスト用クラスここから                             ///
////////////////////////////////////////////////////////////////////////////////
constructor TPerson.Create(name: string; age: integer);
begin
  FName := name;
  FAge  := age;
end;

destructor TPerson.Destroy;
begin
  inherited;
end;

function TPerson.ToString: string;
begin
  result := FName + ',' + FAge.ToString;
end;

function TPersonC.Compare(const Left, Right: TPerson): integer;
begin
  result := (left.GetHashCode - right.GetHashCode) + (Left.Fage - Right.Fage);
end;

function TPersonEC.Equals(const Left, Right: TPerson): boolean;
begin
  if (left = nil) and (right = nil) then begin
    result := true;
    exit;
  end;
  if ((left <> nil) and (right = nil)) or ((left = nil) and (right <> nil)) then begin
    result := false;
    exit;
  end;

  result := false;
  if left.Fage <> right.Fage then exit;
  if not SameStr(left.Fname, right.Fname) then exit;
  result := true;
end;

function TPersonEC.GetHashCode(const Left: TPerson): integer;
var
  md5: TIdHashMessageDigest5;
  stmp: string;
begin
  md5 := TIdHashMessageDigest5.Create;
  try
    stmp := md5.HashStringAsHex(left.Fage.ToString);
    stmp := stmp + md5.HashStringAsHex(left.Fname);
    result := stmp.GetHashCode;
  finally
    md5.Free;
  end;
end;
////////////////////////////////////////////////////////////////////////////////
///                       テスト用クラスここまで                             ///
////////////////////////////////////////////////////////////////////////////////
end.
