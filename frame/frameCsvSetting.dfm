object CsvSetting: TCsvSetting
  Left = 0
  Top = 0
  Width = 322
  Height = 130
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 3
    Top = 1
    Width = 314
    Height = 126
    Caption = 'GroupBox1'
    TabOrder = 0
    object Label1: TLabel
      Left = 3
      Top = 88
      Width = 92
      Height = 13
      Caption = #20966#29702#23550#35937#12501#12451#12540#12523#12489
    end
    object Label2: TLabel
      Left = 3
      Top = 12
      Width = 56
      Height = 13
      Caption = #21306#20999#12426#25991#23383
    end
    object Label3: TLabel
      Left = 96
      Top = 12
      Width = 72
      Height = 13
      Caption = #12504#12483#12480#12540#25991#23383#21015
    end
    object Label4: TLabel
      Left = 4
      Top = 52
      Width = 58
      Height = 13
      Caption = #12501#12449#12452#12523#36984#25246
    end
    object SelectFilePath: TEdit
      Left = 3
      Top = 66
      Width = 276
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 0
    end
    object SelectFileButton: TButton
      Left = 281
      Top = 64
      Width = 27
      Height = 25
      Caption = '...'
      TabOrder = 1
      OnClick = SelectFileButtonClick
    end
    object TargetField: TComboBox
      Left = 3
      Top = 101
      Width = 145
      Height = 21
      TabOrder = 2
    end
    object SepChar: TComboBox
      Left = 3
      Top = 26
      Width = 75
      Height = 22
      Style = csOwnerDrawFixed
      TabOrder = 3
      Items.Strings = (
        #12459#12531#12510
        #12479#12502)
    end
    object Headers: TEdit
      Left = 112
      Top = 26
      Width = 196
      Height = 21
      Hint = #25351#23450#12377#12427#22580#21512#12399#12289#12459#12531#12510#21306#20999#12426#12391#12504#12483#12480#12540#12434#25351#23450#12375#12390#19979#12373#12356
      Color = clBtnFace
      Enabled = False
      TabOrder = 4
      Text = 'test1,test2,test3'
    end
    object HeaderSelfSetting: TCheckBox
      Left = 96
      Top = 28
      Width = 17
      Height = 17
      TabOrder = 5
      OnClick = HeaderSelfSettingClick
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 259
    Top = 49
  end
end
