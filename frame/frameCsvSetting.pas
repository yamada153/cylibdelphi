{******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibdelphi.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************}
unit frameCsvSetting;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  StringTable;

type
  TCsvSetting = class(TFrame)
    GroupBox1: TGroupBox;
    SelectFilePath: TEdit;
    SelectFileButton: TButton;
    TargetField: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    SepChar: TComboBox;
    Headers: TEdit;
    HeaderSelfSetting: TCheckBox;
    Label3: TLabel;
    Label4: TLabel;
    OpenDialog1: TOpenDialog;
    procedure SelectFileButtonClick(Sender: TObject);
    procedure HeaderSelfSettingClick(Sender: TObject);
  private
    { Private 宣言 }
    FSt: TStringTable;
  public
    { Public 宣言 }
    property St: TStringTable read FSt;
    procedure Init(titlestr: string);
    procedure Kill;
    function GetTargetField: string;
  end;

implementation

{$R *.dfm}

function TCsvSetting.GetTargetField: string;
begin
  result := TargetField.Text;
end;

procedure TCsvSetting.HeaderSelfSettingClick(Sender: TObject);
begin
  if HeaderSelfSetting.Checked then begin
    Headers.Color := clWhite;
    Headers.Enabled := true;
  end else begin
    Headers.Color := clBtnFace;
    Headers.Enabled := false;
  end;
end;

procedure TCsvSetting.Init(titlestr: string);
begin
  FSt := nil;
  GroupBox1.Caption := titlestr;
end;

procedure TCsvSetting.Kill;
begin
  FreeAndNil(FSt);
end;

procedure TCsvSetting.SelectFileButtonClick(Sender: TObject);
var
  delimiter: char;
  headerstr: string;
  fields: TArray<string>;
  i: integer;
  s: string;
begin
  if not OpenDialog1.Execute() then exit;
  SelectFilePath.Text := OpenDialog1.FileName;

  if FSt <> nil then FSt := nil;
  //区切り文字設定
  if SepChar.Text = 'カンマ' then begin
    delimiter := ',';
  end else begin
    delimiter := #9;
  end;
  //テキスト読込み
  if HeaderSelfSetting.Checked then begin
    headerstr := Headers.Text;
    fields := headerstr.Split([',']);
    FSt := TStringTable.Create(SelectFilePath.Text, delimiter, fields);
  end else begin
    FSt := TStringTable.Create(SelectFilePath.Text, delimiter);
  end;
  //ターゲットフィールド設定
  fields := FSt.GetFieldNames;
  TargetField.Clear;
  for s in fields do
    TargetField.Items.Add(s);
  TargetField.Text := TargetField.Items[0];
end;

end.
