{******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada
 *  https://yamada153@bitbucket.org/yamada153/cylibcsを参考にしています。
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************}
unit StrFunc;

interface

uses
System.SysUtils, System.Variants, System.Classes, Generics.Collections, Generics.Defaults, types, strutils;

type
TStrFunc = class
private
public
  class function BytesToString(p: TArray<char>): string;
  class function Join(strs: TArray<string>; delimiter: char): string;
  class function TrimStart(str: string; target: Char): String;
  class function TrimEnd(str: string; target: Char): String;
end;

implementation

class function TStrFunc.BytesToString(p: TArray<char>): string;
var
  s: string;
begin
  if High(p) < 0 then begin
    result := '';
    exit;
  end;

  setlength(s, High(p) + 1);
  StrLCopy(PChar(s), PChar(@p[0]), High(p) + 1);
  result := s;
end;

class function TStrFunc.TrimStart(str: string; target: Char): String;
var
  len: integer;
  tmp: string;
begin
  result := str;

  len := Length(result);
  if len = 0 then exit;
  tmp := result[1];
  if result[1] <> target then exit;

  Delete(result, 1, 1);
end;

class function TStrFunc.TrimEnd(str: string; target: Char): String;
var
  len: integer;
  tmp: string;
begin
  result := str;

  len := Length(result);
  if len = 0 then exit;
  tmp := result[len];
  if result[len] <> target then exit;

  Delete(result, len, 1);
end;

class function TStrFunc.Join(strs: TArray<string>; delimiter: char): string;
var
  i: integer;
begin
  result := '';

  for i := 0 to High(strs) do
    result := result + strs[i] + delimiter;
  result := TrimEnd(result, delimiter);
end;

end.
