{******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada
 *  https://yamada153@bitbucket.org/yamada153/cylibcsを参考にしています。
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************}
unit ArrayFunc;

interface

uses
  Classes, Types, SysUtils, System.Generics.Collections, System.Generics.Defaults,
  SSIndex;

type
TArrayFunc = class
public
  // 配列のサイズを変更する
  class function Resize<T>(datas: array of T; size: integer): TArray<T>;
  // 配列をコピーする(インスタンスをコピーするわけじゃないよ!!)
  class function Copy<T>(datas: array of T): TArray<T>;
  // 配列に含まれる指定した要素の数を数える
  class function Count<T>(datas: array of T; target: T; eq: IEqualityComparer<T> = nil): integer;
  // 配列の順序を逆にする
  class procedure Reverse<T>(var datas: array of T);
  // 配列に指定した要素が含まれるならTrueを返す
  class function Contains<T>(datas: array of T; target: T; eq: IEqualityComparer<T> = nil): boolean;
  // 配列の先頭を変更する
  class function Rotate<T>(datas: array of T; newtop: integer): TArray<T>;
  // 配列からnilを取り除く
  class function Compaction<T>(datas: array of T; eq: IEqualityComparer<T>): TArray<T>;
  // 配列を連結する
  class function Concat<T>(a, b: array of T): TArray<T>;
  // 配列から重複を取り除く。元の順番は保たれる
  class function Distinct<T>(datas: array of T; eq: IEqualityComparer<T> = nil): TArray<T>;
  // aにもbにも存在する要素の配列を返す
  class function AndSet<T>(a, b: array of T; eq: IEqualityComparer<T> = nil): TArray<T>;
  // bに存在しないaの要素の配列を返す
  class function SubSet<T>(a, b: array of T; eq: IEqualityComparer<T> = nil): TArray<T>;
  // 配列を指定した要素で区切る
  class function Split<T>(ar: array of T; delimiter: T; eq: IEqualityComparer<T> = nil): TArray<TArray<T>>;
  // 配列から重複する値を取得する
  class function Double<T>(datas: array of T; eq: IEqualityComparer<T> = nil): TArray<T>;
  // 配列の要素が同じなら1を返す。逆にすれば一致するなら2を返す。一致しなければ-1を返す
  class function Equal<T>(a, b: array of T; eq: IEqualityComparer<T> = nil): integer;
  // array of T をTArray<T>に変換する（必要かなぁ？）
  class function ToTArray<T>(datas: array of T): TArray<T>;
end;

implementation


{ TArrayFunc }

class function TArrayFunc.Count<T>(datas: array of T; target: T; eq: IEqualityComparer<T>): integer;
var
  i: integer;
begin
  if eq = nil then eq := TEqualityComparer<T>.Default;
  result := 0;

  for i := 0 to High(datas) do
    if eq.Equals(datas[i], target) then inc(result);
end;

class function TArrayFunc.Contains<T>(datas: array of T; target: T; eq: IEqualityComparer<T>): boolean;
var
  i: integer;
begin
  if eq = nil then eq := TEqualityComparer<T>.Default;

  result := false;

  for i := 0 to High(datas) do
    if eq.Equals(datas[i], target) then result := true;
end;

class function TArrayFunc.Copy<T>(datas: array of T): TArray<T>;
var
  i: integer;
begin
  setlength(result, Length(datas));
  for i := 0 to High(datas) do result[i] := datas[i];
end;

class function TArrayFunc.Resize<T>(datas: array of T; size: integer): TArray<T>;
var
  i: integer;
begin
  setlength(result, size);

  for i := 0 to High(datas) do
    result[i] := datas[i];
end;

class procedure TArrayFunc.Reverse<T>(var datas: array of T);
var
 i, cnt: integer;
 tmp: T;
begin
  cnt := High(datas) div 2;
  for i := 0 to cnt do begin
    tmp := datas[i];
    datas[i] := datas[High(datas) - i];
    datas[High(datas) - i] := tmp;
  end;
end;

class function TArrayFunc.Rotate<T>(datas: array of T; newtop: integer): TArray<T>;
var
  top, under: TArray<T>;
begin
  setlength(top, Length(datas) - newtop);
  setlength(under, newtop);

  TArray.Copy<T>(datas, top,  newtop, 0, Length(datas) - newtop);
  TArray.Copy<T>(datas, under,0,      0, newtop);

  result := Concat(top, under);
end;

class function TArrayFunc.Compaction<T>(datas: array of T; eq: IEqualityComparer<T>): TArray<T>;
var
  tmp: TList<T>;
  i: integer;
begin
  tmp := TList<T>.Create;
  try
    tmp.Capacity := Length(datas);
    for i := 0 to High(datas) do begin
      if eq.Equals(datas[i], T(nil)) then continue;
      tmp.Add(datas[i]);
    end;

    result := tmp.ToArray();
  finally
    tmp.Free;
  end;
end;

class function TArrayFunc.Concat<T>(a, b: array of T): TArray<T>;
begin
  setlength(result, Length(a) + Length(b));
  TArray.Copy<T>(a, result, Length(a));
  TArray.Copy<T>(b, result, 0, Length(a), Length(b));
end;

class function TArrayFunc.Distinct<T>(datas: array of T; eq: IEqualityComparer<T>): TArray<T>;
var
  ss: TSSIndex<T>;
  indexs: TArray<integer>;
  i, j: integer;
begin
  if eq = nil then eq := TEqualityComparer<T>.Default;

  result := TArrayFunc.Copy(datas);

  ss := TSSIndex<T>.Create(datas, eq, TComparer<T>.Default);
  try
    for i := 0 to High(result) do begin
      if eq.Equals(result[i], T(nil)) then continue;

      indexs := ss.Search(result[i]);
      for j := 1 to High(indexs) do
        result[indexs[j]] := T(nil);
    end;
    result := Compaction<T>(result, eq);
  finally
    ss.Free;
  end;
end;

class function TArrayFunc.AndSet<T>(a, b: array of T; eq: IEqualityComparer<T>): TArray<T>;
var
  tmp: TList<T>;
  i: integer;
begin
  if eq = nil then eq := TEqualityComparer<T>.Default;

  tmp := TList<T>.Create;
  try
    tmp.Capacity := Length(a);

    for i := 0 to High(a) do
      if Contains<T>(b, a[i], eq) then
        tmp.Add(a[i]);

    result := tmp.ToArray();
  finally
    tmp.Free;
  end;
end;

class function TArrayFunc.SubSet<T>(a, b: array of T; eq: IEqualityComparer<T>): TArray<T>;
var
  tmp: TList<T>;
  i: integer;
begin
  if eq = nil then eq := TEqualityComparer<T>.Default;

  tmp := TList<T>.Create;
  try
    tmp.Capacity := Length(a);

    for i := 0 to High(a) do
      if not Contains<T>(b, a[i], eq) then
        tmp.Add(a[i]);

    result := tmp.ToArray();
  finally
    tmp.Free;
  end;
end;

class function TArrayFunc.ToTArray<T>(datas: array of T): TArray<T>;
var
  i: integer;
  size: integer;
begin
  size := High(datas)+1;
  setlength(result, size);
  for i := 0 to High(datas) do
    result[i] := datas[i];
end;

class function TArrayFunc.Split<T>(ar: array of T; delimiter: T; eq: IEqualityComparer<T>): TArray<TArray<T>>;
var
  cnt, i: integer;
  set_count: integer;
  tmp: array of TList<T>;
begin
  if eq = nil then eq := TEqualityComparer<T>.Default;

  cnt := Count<T>(ar, delimiter, eq);
  setlength(tmp, cnt + 1);
  for i := 0 to cnt do
    tmp[i] := TList<T>.Create;
  try

    set_count := 0;
    for i := 0 to High(ar) do begin
      if eq.Equals(ar[i], delimiter) then begin
        inc(set_count);
        continue;
      end;

      tmp[set_count].Add(ar[i]);
    end;

    setlength(result, cnt + 1);
    for i := 0 to cnt do
      result[i] := tmp[i].ToArray;
  finally
    for i := 0 to cnt do
      tmp[i].Free;
  end;

end;

class function TArrayFunc.Double<T>(datas: array of T; eq: IEqualityComparer<T>): TArray<T>;
var
  ss: TSSIndex<T>;
  keys: TArray<T>;
  tmp: TList<T>;
  i: integer;
  indexs: TArray<integer>;
  icomp: IComparer<T>;
begin
  if eq = nil then eq := TEqualityComparer<T>.Default;

  ss := TSSIndex<T>.Create(datas, eq, icomp);
  try
    tmp := TList<T>.Create;
    try
      keys := ss.Keys;
      for i := 0 to High(keys) do begin
        indexs := ss.Search(keys[i]);
        if High(indexs) <= 0 then continue;
        tmp.Add(keys[i]);
      end;

      result := tmp.ToArray;
    finally
      tmp.Free;
    end;
  finally
    ss.Free;
  end;
end;

class function TArrayFunc.Equal<T>(a, b: array of T; eq: IEqualityComparer<T>): integer;
var
 i: integer;
begin
  if eq = nil then eq := TEqualityComparer<T>.Default;

  if Length(a) <> Length(b) then begin
    result := -1;
    exit;
  end;

  //そのままで一致する？
  result := 1;
  for i := 0 to high(a) do begin
    if not eq.Equals(a[i], b[i]) then
      result := -1;
  end;
  if result = 1 then exit;

  //逆にすれば一致する？
  result := 2;
  Reverse<T>(b);
  for i := 0 to high(a) do begin
    if not eq.Equals(a[i], b[i]) then
      result := -1;
  end;
  if result = 2 then exit;
end;

end.

