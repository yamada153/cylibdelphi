{******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada
 *  https://yamada153@bitbucket.org/yamada153/cylibcsを参考にしています。
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************}
unit StringTable;

interface

uses
  System.SysUtils, System.Variants, System.Classes, System.types, System.strutils, math,
  System.Generics.Collections, System.Generics.Defaults,

  ArrayFunc, StrFunc;

type

TObjectDynArray = array of TObject;

TStringTable = class
private
  //フィールド名のリスト
  FFields: TList<string>;
  //フィールド名からテーブルの列番号を高速に検索するための辞書
  FFieldsDic: TDictionary<string, integer>;
  //テーブル本体
  FValues: TList<TList<string>>;

  //コンストラクタで共通処理まとめた関数
  procedure Init(fieldnames: array of string; capacity: integer);
  //FFieldsDicを設定する
  procedure SetDic;
  //重複チェック
  function IsDuplicate(fieldname: string): boolean;
  //
  //
  function CheckFieldName(functionname, fieldname: string): integer;
  //
  function GetCsvStringFromFile(sr: TStreamReader; delimiter: Char; capacity: integer = 100): TArray<string>;
  //
  function SeperateToString(strs: TArray<string>; delimiter: Char): string;
public
  //コンストラクタ
  constructor Create(filename: string; delimiter: char; fieldnames : TArray<string> = nil; capacity: integer = 100); overload;
  constructor Create(fieldnames : array of string; capacity: integer = 100); overload;
  constructor Create(st: TStringTable); overload;
  //デストラクタ
  destructor  Destroy;override;
  //ファイルに保存する
  procedure SaveToFile(filename: string; delimiter: Char; field: boolean = true);
  //文字列に変換する
  function ToString(delimiter: Char = ','): string;
  //行数を返す
  function Count: Integer;
  //テーブルの末尾にitemsを追加する
  procedure Add(items: array of string);
  //指定された位置の値を返す
  function GetValue(row: integer; fieldname: string): string;
  //指定された位置に値を格納する
  procedure SetValue(row: integer; fieldname, value: string);
  //指定した行の値を返す
  function GetStringsFromRowNumber(row: integer): TArray<string>;
  //指定した列の値を返す
  function GetStringsFromColumnName(fieldname: string): TArray<string>;
  //フィールド名を返す
  function GetFieldNames: TArray<string>;
  //行を挿入する。指定した行番号が0以下なら0に、最大行番号以上なら最大行番号になる。
  procedure InsertRow(row: integer; items: array of string);
  //行を削除する
  procedure DeleteRow(row: integer);
  //フィールド名を変更する
  procedure ChangeField(fieldname, newfieldname: string);
  //フィールドを削除する
  procedure DeleteField(fieldname: string);
  //フィールドをindexの次の列に挿入する
  procedure InsertField(index: integer; fieldname: string);
  //フィールド名から列番号を返す
  function IndexOf(fieldname: string): integer;
  //フィールド名が存在すればtrueを返す
  function FieldExists(fieldname: string): boolean;
  //インデクサ
  property Data[row: integer; field: string]: string read GetValue write SetValue; default;

end;

implementation

{ TStringTable }

function TStringTable.ToString(delimiter: Char = ','): string;
var
  i: integer;
begin
  result := TStrFunc.Join(GetFieldNames, delimiter) + #13#10;

  for i := 0 to Count - 1 do
    result := result + TStrFunc.Join(GetStringsFromRowNumber(i), delimiter) + #13#10;

  result := Copy(result, 0, Length(result) - 1);
end;

constructor TStringTable.Create(filename: string; delimiter: char; fieldnames : TArray<string>; capacity: integer);
var
  sr: TStreamReader;
  strs: TArray<string>;
  first: boolean;
begin
  first := false;

  sr := TStreamReader.Create(filename, TEncoding.Default);
  try

    while not sr.EndOfStream do begin

      strs := GetCsvStringFromFile(sr, delimiter);
      if strs = nil then break;
      if not first then begin
        first := true;
        try
          if fieldnames <> nil then
            Init(fieldnames, capacity)
          else
            Init(strs, capacity);
        except
          on E: Exception do begin
            raise EInvalidOpException.Create('StringTableのコンストラクタで失敗しました。' + #10#13 + '詳細 : ' + E.Message);
          end;
        end;
      end else begin
        Add(strs);
      end;
    end;

  finally
    sr.Free;
  end;
end;

constructor TStringTable.Create(fieldnames: array of string; capacity: integer);
begin
  Init(fieldnames, capacity);
end;

constructor TStringTable.Create(st: TStringTable);
var
  i: integer;
begin
  Init(st.GetFieldNames, st.Count);

  for i := 0 to st.Count - 1 do
    Add(st.GetStringsFromRowNumber(i));
end;

function TStringTable.GetCsvStringFromFile(sr: TStreamReader; delimiter: Char; capacity: integer = 100): TArray<string>;
var
  infield: boolean;
  stmp: string;
  ctmp: Char;
  buf: byte;
  readbufsize: integer;
  cellvalue: TList<char>;
  cells: TList<string>;
  ch: Char;
  tmp: array of char;
begin
  infield := false; //ダブルクォーテーションから出入りする度に変わる。そのとき以外では使わない

  cellvalue := TList<char>.Create;
  try
    cellvalue.Capacity := 256; //適当
    cells := TList<string>.Create;
    try
      cells.Capacity := 32; //適当

      while sr.Peek > -1 do begin
        ch := Char(sr.Read);

        ////////////////////区切り文字////////////////////
        if delimiter = ch then begin
          if infield then
            cellvalue.Add(ch)
          else begin
            stmp := TStrFunc.TrimStart(TStrFunc.TrimEnd(Trim(TStrFunc.BytesToString(cellvalue.ToArray)), '"'), '"');
            cells.Add(stmp);
            cellvalue.Clear();
          end;
        end
        ///////////////////改行//////////////////////////
        else if (#10 = ch) or
          ((#13 = ch) and (sr.Peek <> 10)) then begin //改行が「\r」だけのテキストがあったので...
          if sr.Peek <= -1 then break; //ファイルの最後の改行

          if infield then begin
            ctmp := #13;
            cellvalue.Add(ctmp);
            ctmp := #10;
            cellvalue.Add(ctmp);
            continue;
          end;

          stmp := TStrFunc.TrimStart(TStrFunc.TrimEnd(Trim(TStrFunc.BytesToString(cellvalue.ToArray)), '"'), '"');
          cells.Add(stmp);
          result := cells.ToArray;
          exit;
        end
        ///////////////////ダブルクォーテーション///////////////////
        else if '"' = ch then begin
          //フィールドの最初が「"」のとき
          if not infield then begin
            infield := true;
            cellvalue.Add(ch);
          end
          //フィールド内に「"」が見つかり、次の文字も「"」の場合
          else if infield and (Chr(sr.Peek) = '"') then begin
            cellvalue.Add(ch);
            sr.Read; //次のダブルクォーテーションは捨てる
          end
          //フィールドの最後!?�@
          else if infield and ((Chr(sr.Peek) = ' ') or (Chr(sr.Peek) = #9) or (Chr(sr.Peek) = delimiter)) then begin
            infield := false;
            cellvalue.Add(ch);
          end
          //フィールドの最後!?�A
          else if infield and (Chr(sr.Peek) = #13) then begin
            sr.Read; //\r捨てる
            infield := false;
            cellvalue.Add(ch);
          end;
        end
        ///////////////////スペース///////////////////
        else if (' ' = ch) or (#9 = ch) then begin
          if infield or (cellvalue.Count <> 0) then
            cellvalue.Add(ch);
        end
        ///////////////////キャリッジリターン///////////////////
        else if #13 = ch then begin
        end
        ///////////////////上記以外の文字///////////////////
        else begin
          cellvalue.Add(ch);
        end;
      end;
      //最後のレコードが改行で終わっていない場合、未処理のセルが溜まっているので処理する
        if cellvalue.Count = 0 then begin
          result := cells.ToArray(); //フィールドは全部出来てる
          exit;
        end;

        cells.Add(TStrFunc.TrimEnd(TStrFunc.TrimStart(TStrFunc.BytesToString(cellvalue.ToArray), '"'), '"')); //フィールド途中でファイルが終わっている場合
        result := cells.ToArray;
        exit;

      result := nil;
    finally
      cells.Free;
    end;
  finally
    cellvalue.Free;
  end;
end;

destructor TStringTable.Destroy;
var
  i: integer;
begin
  if FValues = nil then exit; //コンストラクタで例外が発生した場合

  for i := 0 to FValues.Count - 1 do begin
    FValues[i].Free;
  end;
  FValues.Free;
  FFieldsDic.Free;
  FFields.Free;
  inherited;
end;

function TStringTable.Count: Integer;
begin
  result := FValues[0].Count;
end;

// itemsの要素数がFFieldより多い場合は、多い分が切り捨てられる。
// itemsの要素数がFFieldより少ない場合は、空文字列が充てられる。
procedure TStringTable.Add(items: array of string);
var
  i, cnt: integer;
  tmp: TArray<string>;
begin
  tmp := TArrayFunc.ToTArray(items);
  setlength(tmp, FFields.Count);

  for i := 0 to FFields.Count - 1 do
    FValues[i].Add(tmp[i]);
end;

function TStringTable.GetValue(row: integer; fieldname: string): string;
var
  col: integer;
begin
  col := IndexOf(fieldname);
  result := FValues[col][row];
end;

procedure TStringTable.SetValue(row: integer; fieldname, value: string);
var
  col: integer;
begin
  col := IndexOf(fieldname);
  FValues[col][row] := value;
end;

procedure TStringTable.InsertRow(row: integer; items: array of string);
var
  i: integer;
  tmp: TArray<string>;
begin
  tmp := TArrayFunc.Resize<string>(items, FFields.Count);

  if (row > (FValues[0].Count - 1)) or (FValues[0].Count = 0) then begin
    Add(tmp);
    exit;
  end;

  row := EnsureRange(row, 0, FFields.Count - 1);

  for i := 0 to FFields.Count - 1 do
    FValues[i].Insert(row, tmp[i]);
end;

procedure TStringTable.DeleteRow(row: integer);
var
  i: integer;
begin
  if EnsureRange(row, 0, FValues[0].Count - 1) <> row then
    raise Exception.Create('行番号は0〜' + inttostr(FValues[0].Count - 1) + 'の間で指定して下さい');

  for i := 0 to FFields.Count - 1 do
    FValues[i].Delete(row);
end;

procedure TStringTable.ChangeField(fieldname, newfieldname: string);
var
  index: integer;
begin
  if newfieldname.Trim = '' then
    raise EArgumentException.Create('TStringTable.ChangeFieldの引数newfieldnameに空文字か空白文字が指定されました');

  index := IndexOf(fieldname);
  FFields[index] := newfieldname;
  SetDic;
end;

procedure TStringTable.DeleteField(fieldname: string);
var
  index: integer;
begin
  if fieldname.Trim = '' then
    raise EArgumentException.Create('TStringTable.DeleteFieldの引数fieldnameに空文字か空白文字が指定されました');

  index := IndexOf(fieldname);
  FFields.Delete(index);
  SetDic;
  FValues[index].Free;
  FValues.Delete(index);
end;

procedure TStringTable.InsertField(index: integer; fieldname: string);
var
  l: TList<string>;
  i: integer;
begin
  index := EnsureRange(index, 0, FFields.Count); //FFields.Count-1 でないのは間違いではない

  FFields.Insert(index, fieldname);
  SetDic;

  l := TList<string>.Create;
  for i := 0 to Count - 1 do
    l.Add('');
  FValues.Insert(index, l);
end;

function TStringTable.IndexOf(fieldname: string): integer;
begin
  result := -1;
  if not FFieldsDic.TryGetValue(fieldname, result) then
    raise EInvalidArgument.Create('指定されたフィールド名「' + fieldname + '」は存在しません');
end;

procedure TStringTable.Init(fieldnames: array of string; capacity: integer);
var
  s: string;
  i: integer;
  tmp: TList<string>;
begin
  FFields := TList<string>.Create();
  FFields.Capacity := 25; //適当
  FFieldsDic := TDictionary<string,integer>.Create(FFields.Capacity);
  for i := 0 to High(fieldnames) do
    FFields.Add(fieldnames[i]);
  SetDic;

  FValues := TList<TList<string>>.Create;
  FValues.Capacity := High(fieldnames) + 1;
  for  i := 0 to High(fieldnames) do begin
    tmp := TList<string>.Create;
    tmp.Capacity := capacity;
    FValues.Add(tmp);
  end;
end;

function TStringTable.IsDuplicate(fieldname: string): boolean;
begin
  result := FFieldsDic.ContainsKey(fieldname);
end;

procedure TStringTable.SetDic;
var
  i: integer;
  s: string;
  newdic: TDictionary<string, integer>;
begin
  //空白文字か空文字は受け取れない
  for i := 0 to FFields.Count - 1 do begin
    if FFields[i].Trim = '' then
      raise EArgumentException.Create('フィールドの' + (i + 1).ToString + '番目の文字が空か空白文字です');
  end;


  FFieldsDic.Clear;
  try
    for i := 0 to FFields.Count - 1 do begin
      FFieldsDic.Add(FFields[i], i);
    end;
  except
    on e:EListError do raise Exception.Create('フィールド名が重複しています。「' + TStrFunc.Join(FFields.ToArray, ',') + '」');
  end;
end;

function TStringTable.GetStringsFromRowNumber(row: integer): TArray<string>;
var
  i, n: integer;
begin
  n := High(GetFieldNames) + 1;
  setlength(result, n);

  for i := 0 to n - 1 do
    result[i] := FValues[i][row];
end;

function TStringTable.GetStringsFromColumnName(fieldname: string): TArray<string>;
var
  index: integer;
begin
  index := CheckFieldName('GetStringsFromColumnName', fieldname);

  result :=  FValues[index].ToArray;
end;

function TStringTable.FieldExists(fieldname: string): boolean;
begin
  if not FFieldsDic.ContainsKey(fieldname) then
    result := false
  else
    result := true;
end;

function TStringTable.GetFieldNames: TArray<string>;
begin
  result := FFields.ToArray;
end;

function TStringTable.CheckFieldName(functionname, fieldname: string): integer;
var
  index: integer;
begin
  if fieldname = '' then
    raise EInvalidArgument.Create('StringTable.' + functionname + 'の引数「fieldname」が空文字列を受け取りました。');

  index := IndexOf(fieldname);
  if index = -1 then raise EInvalidArgument.Create('StringTable.' + functionname + 'の引数「fieldname('+ fieldname +')」は存在しません。');

  result := index;
end;

procedure TStringTable.SaveToFile(filename: string; delimiter: Char; field: boolean);
var
  tf: TextFile;
  i: integer;
begin
  AssignFile(tf, filename);
  Rewrite(tf);
  try
    if field then
      Writeln(tf, SeperateToString(GetFieldNames(), delimiter));

    for i := 0 to Count() - 1 do
      Writeln(tf, SeperateToString(GetStringsFromRowNumber(i), delimiter));
  finally
    CloseFile(tf);
  end;
end;

function TStringTable.SeperateToString(strs: TArray<string>; delimiter: Char): string;
var
  i: integer;
  tmp: string;
begin
  result := '';

  for i := 0 to High(strs) do begin
    tmp := strs[i];
    if tmp.Contains(#10) then
      tmp := '"' + tmp + '"'
    else if tmp.Contains(delimiter) then
      tmp := '"' + tmp + '"'
    else if tmp.Contains(' ') then
      tmp := '"' + tmp + '"'
    else if tmp.Contains(#9) then
      tmp := '"' + tmp + '"'
    else if tmp.Contains('"') then begin
      tmp := AnsiReplaceStr(tmp, '"', '""');
      tmp := '"' + tmp + '"';
    end;
    result := result + tmp + delimiter;
  end;
  result := TStrFunc.TrimEnd(result, delimiter);
end;

end.

