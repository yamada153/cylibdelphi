{******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada
 *  https://yamada153@bitbucket.org/yamada153/cylibcsを参考にしています。
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************}
unit SSIndex;

interface

uses
  System.SysUtils, System.Variants, System.Classes, Generics.Collections, Generics.Defaults, types, math, strutils;

type TSSIndex<T> = class
private
  FIndexRecord: TDictionary<T,TList<integer>>;
  FSearchComp:  IEqualityComparer<T>;
  FSortComp:    IComparer<T>;
public
  //コンストラクタ
  constructor Create(datas: array of T; searchcomp: IEqualityComparer<T>; sortcomp: IComparer<T>); overload;
  constructor Create(searchcomp: IEqualityComparer<T>; sortcomp: IComparer<T>; capacity: integer = 100); overload;
  constructor Create(datas: array of T); overload;
  constructor Create(capacity: integer = 100); overload;
  //後始末
  destructor  Destroy;override;
  //キーまたはインデックス追加
  procedure Add(key: T; index: integer);
  //指定したキーに紐付けされたインデックスの配列を返す
  function Search(key: T): TArray<integer>;
  //並び替えて、インデックスの配列を返す。
  function Sort(): TArray<integer>;
  //登録済のキーを返す
  function Keys: TArray<T>;
  //登録済のキーの数を返す
  function Count: Integer;
end;

implementation
{ TSSIndex<T> }

function TSSIndex<T>.Count: integer;
begin
  result := FIndexRecord.Count;
end;

constructor TSSIndex<T>.Create(datas: array of T; searchcomp: IEqualityComparer<T>; sortcomp: IComparer<T>);
var
  i: integer;
begin
  FSearchComp:=  searchcomp;
  FSortComp:=    sortcomp;
  FIndexRecord := TDictionary<T,TList<integer>>.Create(High(datas) + 1, FSearchComp);

  for i := 0 to High(datas) do
    Add(datas[i], i);
end;

constructor TSSIndex<T>.Create(searchcomp: IEqualityComparer<T>; sortcomp: IComparer<T>; capacity: integer);
begin
  FSearchComp:=  searchcomp;
  FSortComp:=    sortcomp;
  FIndexRecord := TDictionary<T,TList<integer>>.Create(capacity, FSearchComp);
end;

constructor TSSIndex<T>.Create(datas: array of T);
var
  i: integer;
begin
  FSearchComp:=  TEqualityComparer<T>.Default;
  FSortComp:=    TComparer<T>.Default;
  FIndexRecord := TDictionary<T,TList<integer>>.Create(High(datas) + 1);

  for i := 0 to High(datas) do
    Add(datas[i], i);
end;

constructor TSSIndex<T>.Create(capacity: integer);
begin
  FSearchComp:=  TEqualityComparer<T>.Default;
  FSortComp:=    TComparer<T>.Default;
  FIndexRecord := TDictionary<T,TList<integer>>.Create(capacity);
end;

destructor TSSIndex<T>.Destroy;
var
  i: integer;
  tmp: T;
begin
  for tmp in FIndexRecord.Keys do
    FIndexRecord[tmp].Free;
  FIndexRecord.Free;
  inherited;
end;

function TSSIndex<T>.Keys: TArray<T>;
begin
  result := FIndexRecord.Keys.ToArray;
end;

procedure TSSIndex<T>.Add(key: T; index: integer);
var
  newindexslist: TList<integer>;
begin
  if not FIndexRecord.ContainsKey(key) then begin
    newindexslist := TList<integer>.Create();
    newindexslist.Add(index);
    FIndexRecord.Add(key, newindexslist);
  end else begin
    FIndexRecord[key].Add(index);
  end;
end;

function TSSIndex<T>.Search(key: T): TArray<integer>;
begin
  if not FIndexRecord.ContainsKey(key) then
    result := nil
  else
    result := FIndexRecord[key].ToArray();
end;


function TSSIndex<T>.Sort: TArray<integer>;
var
  tmp: TArray<T>;
  resultlist: TList<integer>;
  i, j: integer;
  indexs: TArray<integer>;
begin
  tmp := FIndexRecord.Keys.ToArray;
  TArray.Sort<T>(tmp, FSortComp);

  resultlist := TList<integer>.Create;
  try
    resultlist.Capacity := High(tmp) * 2;
    for i := 0 to High(tmp) do begin
      indexs := Search(tmp[i]);
      for j := 0 to High(indexs) do begin
        resultlist.Add(indexs[j]);
      end;
    end;

    result := resultlist.ToArray;
  finally
    resultlist.Free;
  end;
end;

end.
