{******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada
 *  https://yamada153@bitbucket.org/yamada153/cylibcsを参考にしています。
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************}
unit FileFunc;

interface

uses
  Classes, Types, SysUtils, System.Generics.Collections, System.Generics.Defaults, IOUtils, Hash;

type
TFileFunc = class
public
  //ファイルを読み込んで配列に格納する
  class function LoadFromFile(filename: string): TArray<string>;
  //配列をファイルに書き出す
  class procedure SaveToFile(strs: TArray<string>; filename: string);
  //ファイルリストを作成する。flg=trueなら最下層まで探索する
  class function FileList(sParentPath, sRule: String; flg: boolean = false): TArray<string>;
  //フォルダリストを作成する。flg=trueなら最下層まで探索する
  class function FolderList(sParentPath, sRule: String; flg: boolean = false): TArray<string>;
  //指定したファイルのMD5を取得する
  class function GetMD5(filename: string): string;
end;

implementation


{ TFileFunc }

class function TFileFunc.LoadFromFile(filename: string): TArray<string>;
var
  sr: TStreamReader;
begin
  sr := TStreamReader.Create(filename, TEncoding.Default);
  try
    result := sr.ReadToEnd.Split([sLineBreak]);
  finally
    sr.Free;
  end;
end;

class procedure TFileFunc.SaveToFile(strs: TArray<string>; filename: string);
var
  sw: TStreamWriter;
  s: string;
begin
  sw := TStreamWriter.Create(filename, false, TEncoding.Default);
  try
    for s in strs do sw.WriteLine(s);
  finally
    sw.Free;
  end;
end;

class function TFileFunc.FileList(sParentPath, sRule: String; flg: boolean): TArray<string>;
var
  Option: TSearchOption;
begin
  if flg = true then
    Option := TSearchOption.soAllDirectories     // 再帰列挙モード
  else
    Option := TSearchOption.soTopDirectoryOnly; // トップレベル列挙モード
  result := TArray<string>(TDirectory.GetFiles(sParentPath, sRule, Option));
end;

class function TFileFunc.FolderList(sParentPath, sRule: String; flg: boolean): TArray<string>;
var
  Option: TSearchOption;
begin
  if flg = true then
    Option := TSearchOption.soAllDirectories
  else
    Option := TSearchOption.soTopDirectoryOnly;
  result := TArray<string>(TDirectory.GetDirectories(sParentPath, sRule, Option));
end;

class function TFileFunc.GetMD5(filename: string): string;
var
  TS: TFileStream;
  bytes: TBytes;
  MD5: THashMD5;
begin
  TS := TFileStream.Create(filename, fmOpenRead);
  try
    setlength(bytes, TS.Size);
    TS.Read(bytes, High(bytes) + 1);

    MD5 := THashMD5.Create;
    MD5.Update(bytes, High(bytes) + 1);

    result := MD5.HashAsString;
  finally
    TS.Free;
  end;
end;

end.


