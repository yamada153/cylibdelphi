{******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibdelphi.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************}
unit SkeltonMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.IniFiles, FileCtrl,
  SmartPointerClass, Vcl.ComCtrls;

type
  TSkeltonForm = class(TForm)
    LoadFile: TEdit;
    Label1: TLabel;
    LoadFileSelect: TButton;
    Label2: TLabel;
    Memo: TMemo;
    SaveFile: TEdit;
    Label3: TLabel;
    SaveFileSelect: TButton;
    LoadFolder: TEdit;
    Label4: TLabel;
    LoadFolderSelect: TButton;
    SaveFolder: TEdit;
    Label5: TLabel;
    SaveFolderSelect: TButton;
    ExecButton: TButton;
    ExitButton: TButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ProgressBar1: TProgressBar;
    procedure ExitButtonClick(Sender: TObject);
    procedure LoadFolderSelectClick(Sender: TObject);
    procedure SaveFolderSelectClick(Sender: TObject);
    procedure LoadFileSelectClick(Sender: TObject);
    procedure SaveFileSelectClick(Sender: TObject);
    procedure ExecButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private 宣言 }
    procedure Start;
  public
    { Public 宣言 }
  end;

var
  SkeltonForm: TSkeltonForm;

implementation

{$R *.dfm}

procedure TSkeltonForm.Start;
var
  i: integer;
begin
  ProgressBar1.Min := 0;
  ProgressBar1.Max := 100;

  for i := 0 to 100 do begin
    ProgressBar1.Position := i;
    Application.ProcessMessages;


  end;

end;

procedure TSkeltonForm.ExecButtonClick(Sender: TObject);
begin
{
  if not FileExists(LoadFile.Text) then begin
    showmessage('読み込みファイル「' + LoadFile.Text + '」が見つかりませんでした。');
    exit;
  end;

  if not DirectoryExists(LoadFolder.Text) then begin
    showmessage('読み込み元フォルダ「' + LoadFolder.Text + '」が見つかりませんでした。');
    exit;
  end;

  if not DirectoryExists(ExtractFileDir(SaveFile.Text)) then begin
    showmessage('保存ファイル「' + SaveFile.Text + '」の保存先フォルダ' + ExtractFileDir(SaveFile.Text) + 'が見つかりませんでした。');
    exit;
  end;

  if not DirectoryExists(SaveFolder.Text) then begin
    showmessage('保存先フォルダ「' + SaveFolder.Text + '」が見つかりませんでした。');
    exit;
  end;
}
end;

procedure TSkeltonForm.ExitButtonClick(Sender: TObject);
begin
  Close;
end;

procedure TSkeltonForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  try
    ini.WriteString('初期設定', '読み込みファイル', LoadFile.Text);
    ini.WriteString('初期設定', '書き出しファイル', SaveFile.Text);
    ini.WriteString('初期設定', '読み込みフォルダ', LoadFolder.Text);
    ini.WriteString('初期設定', '書き出しフォルダ', SaveFolder.Text);
  finally
    Ini.Free;
  end;
end;

procedure TSkeltonForm.FormCreate(Sender: TObject);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  try
    LoadFile.Text := ini.ReadString('初期設定', '読み込みファイル', '');
    SaveFile.Text := ini.ReadString('初期設定', '書き出しファイル', '');
    LoadFolder.Text:=ini.ReadString('初期設定', '読み込みフォルダ', '');
    SaveFolder.Text:=ini.ReadString('初期設定', '書き出しフォルダ', '');
  finally
    Ini.Free;
  end;
end;

procedure TSkeltonForm.LoadFileSelectClick(Sender: TObject);
begin
  if not OpenDialog1.Execute() then exit;

  LoadFile.Text := OpenDialog1.FileName;
end;

procedure TSkeltonForm.LoadFolderSelectClick(Sender: TObject);
var
  dir: string;
begin
  if not SelectDirectory('読み込み元フォルダの選択', '', dir) then exit;

  LoadFolder.Text := dir;
end;

procedure TSkeltonForm.SaveFileSelectClick(Sender: TObject);
begin
  if not SaveDialog1.Execute() then exit;

  SaveFile.Text := SaveDialog1.FileName;
end;

procedure TSkeltonForm.SaveFolderSelectClick(Sender: TObject);
var
  dir: string;
begin
  if SelectDirectory('保存先フォルダの選択','',dir, [sdNewUI, sdNewFolder], Self) = false then exit;

  SaveFolder.Text := dir;
end;

end.
