program skelton;

uses
  Vcl.Forms,
  SkeltonMain in 'SkeltonMain.pas' {SkeltonForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TSkeltonForm, SkeltonForm);
  Application.Run;
end.
