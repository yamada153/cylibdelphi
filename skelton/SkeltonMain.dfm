object SkeltonForm: TSkeltonForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'SkeltonForm'
  ClientHeight = 230
  ClientWidth = 493
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 5
    Width = 80
    Height = 13
    Caption = #35501#12415#36796#12415#12501#12449#12452#12523
  end
  object Label2: TLabel
    Left = 274
    Top = 8
    Width = 17
    Height = 13
    Caption = #12513#12514
  end
  object Label3: TLabel
    Left = 8
    Top = 45
    Width = 58
    Height = 13
    Caption = #20445#23384#12501#12449#12452#12523
  end
  object Label4: TLabel
    Left = 8
    Top = 85
    Width = 93
    Height = 13
    Caption = #35501#12415#36796#12415#20803#12501#12457#12523#12480
  end
  object Label5: TLabel
    Left = 8
    Top = 125
    Width = 71
    Height = 13
    Caption = #20445#23384#20808#12501#12457#12523#12480
  end
  object LoadFile: TEdit
    Left = 8
    Top = 24
    Width = 233
    Height = 21
    TabOrder = 0
  end
  object LoadFileSelect: TButton
    Left = 243
    Top = 20
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 1
    OnClick = LoadFileSelectClick
  end
  object Memo: TMemo
    Left = 274
    Top = 27
    Width = 199
    Height = 177
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object SaveFile: TEdit
    Left = 8
    Top = 64
    Width = 233
    Height = 21
    TabOrder = 3
  end
  object SaveFileSelect: TButton
    Left = 243
    Top = 60
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 4
    OnClick = SaveFileSelectClick
  end
  object LoadFolder: TEdit
    Left = 8
    Top = 104
    Width = 233
    Height = 21
    TabOrder = 5
  end
  object LoadFolderSelect: TButton
    Left = 243
    Top = 100
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 6
    OnClick = LoadFolderSelectClick
  end
  object SaveFolder: TEdit
    Left = 8
    Top = 144
    Width = 233
    Height = 21
    TabOrder = 7
  end
  object SaveFolderSelect: TButton
    Left = 243
    Top = 140
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 8
    OnClick = SaveFolderSelectClick
  end
  object ExecButton: TButton
    Left = 48
    Top = 179
    Width = 75
    Height = 25
    Caption = #23455#34892
    TabOrder = 9
    OnClick = ExecButtonClick
  end
  object ExitButton: TButton
    Left = 152
    Top = 179
    Width = 75
    Height = 25
    Caption = #38281#12376#12427
    TabOrder = 10
    OnClick = ExitButtonClick
  end
  object ProgressBar1: TProgressBar
    Left = 8
    Top = 210
    Width = 477
    Height = 14
    TabOrder = 11
  end
  object OpenDialog1: TOpenDialog
    Left = 304
    Top = 16
  end
  object SaveDialog1: TSaveDialog
    Left = 344
    Top = 16
  end
end
